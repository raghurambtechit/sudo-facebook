package facebook.DAO;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.tomcat.jni.File;

import facebook.DatabaseConnection;
import facebook.model.postModel;
public class postDAO {

	public ArrayList getFriendsPost(String username)  {
		try {
			Connection con = DatabaseConnection.initializeDatabase();
			
			PreparedStatement sp = con.prepareStatement("SELECT * FROM friends WHERE username=? ");
			sp.setString(1, username);
		    ResultSet friends = sp.executeQuery();
		    String friend = "";
		   
		  //  ArrayList<postModel> friendspost = new ArrayList<postModel>();
		    
		    ArrayList friendpost = new ArrayList();
		    
		    while(friends.next()) {
		    	friend=friends.getString("friendusername");
			    friendpost.add(getUserPosts(friend));
			    System.out.println("While Friend "+friendpost);
		    }	
		    System.out.println(" Friend "+friendpost);
		    return  friendpost;  
		}
		catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	  }


	private ArrayList getlikes(int postid) throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException {
		Connection con = DatabaseConnection.initializeDatabase();
		
		PreparedStatement sp = con.prepareStatement("SELECT * FROM likes WHERE postid=? ");
		sp.setInt(1, postid);
	    ResultSet rs = sp.executeQuery();
	  
	    ArrayList likes = new ArrayList();
	   
	    while (rs.next()) {
	    	likes.add(rs.getString("username"));
	    }
	    
		return likes;
	}



	private ArrayList getComments(int postid) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		Connection con = DatabaseConnection.initializeDatabase();
	
		PreparedStatement sp = con.prepareStatement("SELECT * FROM comments WHERE postid=? ");
		sp.setInt(1, postid);
	  
		ResultSet rs = sp.executeQuery();
	   
		ArrayList comments = new ArrayList();
	  
	    while (rs.next()) {
	    	comments.add(rs.getString("comment"));
	    }
	    
		return comments;
	}



	public boolean dolike(String username, int postid) {
		boolean result = false;
		try {
			Connection con = DatabaseConnection.initializeDatabase();
		
			PreparedStatement sp = con.prepareStatement("INSERT INTO likes (username,postid) VALUES (?,?)");
			PreparedStatement ps = con.prepareStatement("UPDATE post SET likes = likes + 1 WHERE postid = ?");
			sp.setString(1, username);
		    sp.setInt(2, postid);
		    ps.setInt(1, postid);
		  
		    int rs = sp.executeUpdate();
		    int likeupdate = ps.executeUpdate();
		    
		    if (rs == 1 && likeupdate ==1) {
		    	result = true;
		    }
		    
		}catch(Exception e) {
			result = false;
		}
		return result;
	}

	public boolean doComment(String username, int postid, String comment) {
		boolean result = false;
		try {
			Connection con = DatabaseConnection.initializeDatabase();
		
			PreparedStatement sp = con.prepareStatement("INSERT INTO comments (username,postid,comment) VALUES (?,?,?)");
			PreparedStatement ps = con.prepareStatement("UPDATE post SET comments = comments + 1 WHERE postid = ?");
		
			sp.setString(1, username);
		    sp.setInt(2, postid);
		    sp.setString(3, comment);
		    
		    ps.setInt(1, postid);
		    int rs = sp.executeUpdate();
		    int likeupdate = ps.executeUpdate();
		    
		    if (rs == 1 && likeupdate ==1) {
		    	result = true;
		    }
		    
		}catch(Exception e) {
			result = false;
		}
		return result;
	}

	

	public boolean uploadImg(String username, int postid , InputStream inputStream , int likes , int comments ) {
		boolean result = false;
		try {
		
			Connection con = DatabaseConnection.initializeDatabase();
			System.out.println(inputStream );
			String message = null;
			PreparedStatement pstmtSave = con.prepareStatement("INSERT INTO post (postid,file,username,likes,comments) VALUES (?,?,?,?,?)");
		
			pstmtSave.setInt(1, postid);
			pstmtSave.setString(3, username);
			pstmtSave.setInt(4,likes);
			pstmtSave.setInt(5,comments);
			if (inputStream != null) {
	             pstmtSave.setBlob(2, inputStream);
	         }
	
			System.out.println(username  );
			
			int row = pstmtSave.executeUpdate();
	         if (row > 0) {
	             message = "File uploaded and saved into database";
	             result = true;
	             System.out.println(message);
	         }
		}catch(Exception e) {
			result = false;
		}
		return result;
	}


	
//	public ArrayList<postModel> getUserPosts(String username)  {
//		try {
//			Connection con = DatabaseConnection.initializeDatabase();
//			PreparedStatement sp = con.prepareStatement("SELECT * FROM post WHERE username=? ");
//			sp.setString(1, username);
//		    ResultSet rs = sp.executeQuery();
//		    //ArrayList<postModel> userPosts  = new ArrayList<postModel>();
//   
//		    ArrayList userPosts = new ArrayList();
//		   
//		    while(rs.next()) {
//		    	ArrayList al = new ArrayList();
//		    	al.add(rs.getInt("postid"));
//		    	al.add(rs.getInt("likes"));
//		    	al.add(rs.getInt("comments"));
//		    	al.add(rs.getString("username"));
//		    	al.add(rs.getBlob("file"));
//		    	al.add(getComments(rs.getInt("postid")));
//		    	al.add(getlikes(rs.getInt("postid")));
//		    	userPosts.add(al);
//		    }
//		    		   
//		   return  userPosts;
//		    
//		}catch(Exception e) {
//			e.printStackTrace();
//			return null;
//		}
//	  }         (OR)

	public ArrayList<postModel> getUserPosts(String username) {
		try {
			Connection con = DatabaseConnection.initializeDatabase();
			PreparedStatement sp = con.prepareStatement("SELECT * FROM post WHERE username=? ");
			sp.setString(1, username);
		    ResultSet rs = sp.executeQuery();
		    ArrayList<postModel> userPosts  = new ArrayList<>();
		    postModel pm = new postModel(username, 0, null, 0, 0);
		    
		
		    while(rs.next()) {
		    	
		    	pm.setPostid(rs.getInt("postid"));
		    	pm.setComments(rs.getInt("comments"));
		    	pm.setFile(rs.getBlob("file"));
		    	pm.setLikes(rs.getInt("likes"));
		    	pm.setUsername(rs.getString("username"));
		    	
		    	userPosts.add(pm);
		  //  	System.out.println(userPosts.get(0).getPostid());
		    	
		    }
		   
		   

		    return userPosts ;
		    
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	
	
	
}