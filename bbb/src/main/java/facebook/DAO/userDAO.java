package facebook.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import facebook.DatabaseConnection;
import facebook.model.postModel;

public class userDAO {
	
	public boolean login(String name , String password) {
		boolean result = false;
		try {
			Connection con = DatabaseConnection.initializeDatabase();
			
			PreparedStatement sp = con.prepareStatement("SELECT * FROM userauth WHERE username=? and password = ?");
			
			sp.setString(1, name);
		    sp.setString(2, password);
		    ResultSet rs = sp.executeQuery();
		    
		    if (rs.next()) {
		    	if(rs.getString("username").equals(name) && rs.getString("password").equals(password)) {
		    		result = true;
		    	}
		    }
		    
		    
		}catch(Exception e) {
			result = false;
		}
		return result;
	}

	
public boolean register(String name , String password, int userid , String about) {
		boolean result = false;
		try {
			Connection con = DatabaseConnection.initializeDatabase();
			
			PreparedStatement sp = con.prepareStatement("INSERT INTO userauth (username,password,userid,about) VALUES (?,?,?,?)");
			
			sp.setString(1, name);
		    sp.setString(2, password);
		    sp.setInt(3, userid);
		    sp.setString(4, about);
		    int rs = sp.executeUpdate();
		    if (rs == 1) {
		    	result = true;
		    }  
		}catch(Exception e) {
			result = false;
		}
		return result;
	}

 
	public String getAbout(String username) {
		try {
			Connection con = DatabaseConnection.initializeDatabase();
			PreparedStatement sp = con.prepareStatement("SELECT about FROM userauth WHERE username=?");
			sp.setString(1, username);
		    ResultSet aboutuser = sp.executeQuery();
		    
		    if(aboutuser.next()) {
		    	return aboutuser.getString("about");
		    }
		    return null;
		}
		catch(Exception e) {
			return null;
		}
	}
	public boolean addFriend(String username, String friendname) {
		boolean result = false;
		try {
			Connection con = DatabaseConnection.initializeDatabase();
			
			PreparedStatement sp = con.prepareStatement("INSERT INTO friends (username,friendusername,status) VALUES (?,?,?)");
			sp.setString(1, username);
		    sp.setString(2, friendname);
		    sp.setString(3, "friend");
		    int rs = sp.executeUpdate();
		    
		    
			PreparedStatement sp2 = con.prepareStatement("INSERT INTO friends (username,friendusername,status) VALUES (?,?,?)");
			sp2.setString(1, friendname);
		    sp2.setString(2, username);
		    sp2.setString(3, "friend");
		    int rs2 = sp2.executeUpdate();

		    if (rs2 == 1 && rs ==1 ) {
		    	result = true;
		    }
		    
		}catch(Exception e) {
			result = false;
		}
		return result;
	}


	public ArrayList<String> getUserFriends(String username)  {
		try {
			Connection con = DatabaseConnection.initializeDatabase();
			PreparedStatement sp = con.prepareStatement("SELECT * FROM friends WHERE username=? ");
			sp.setString(1, username);
		    ResultSet rs = sp.executeQuery();
		    ArrayList<String> userfriends  = new ArrayList<String>();
		       if(rs.next()) {	
		    	   userfriends.add(rs.getString("friendusername"));
		       }	   
		   
		    return  userfriends;
		    
		}catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	  }	
	
}
