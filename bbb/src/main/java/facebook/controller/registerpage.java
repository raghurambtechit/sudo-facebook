package facebook.controller;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facebook.DatabaseConnection;
import facebook.DAO.userDAO;

@WebServlet(value = "/register") 
public class registerpage extends HttpServlet	{

	 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	 {
		   	
		   RequestDispatcher view = request.getRequestDispatcher("register.jsp");
		   view.forward(request, response);
		   
	   }
	 
	 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 try {
				String username = request.getParameter("username");
				String password =request.getParameter("password");
				String about = request.getParameter("about");
				
				Random rand = new Random(); 
				int userid = rand.nextInt(9000000) + 1000000; 
			
				
				userDAO dao = new userDAO();
				boolean  result = dao.register(username, password,userid,about);
			
		  	if(result) {
		  	
					Cookie ck=new Cookie("userCookie",username);
					response.addCookie(ck);
					
					response.sendRedirect("http://localhost:8080/bbb/");
					
				}	
			}		
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	 
	 
	 
	
	
	
	
	
	
	
	
	
	
	
	
