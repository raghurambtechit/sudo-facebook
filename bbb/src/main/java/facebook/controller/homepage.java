package facebook.controller;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.parser.Cookie;

import facebook.DatabaseConnection;
import facebook.DAO.postDAO;
import facebook.DAO.userDAO;
import facebook.model.postModel;

@WebServlet(value = "/") 
public class homepage extends HttpServlet	{

public void doGet(HttpServletRequest request,	HttpServletResponse response)
	throws ServletException, IOException
{
	javax.servlet.http.Cookie[] ck=request.getCookies();  
		try {

			
			String username =  ck[1].getValue();

			
			response.getWriter().print("Welcome "+username);

			postDAO postdao = new postDAO();
			ArrayList <postModel> result = postdao.getFriendsPost(username);
			
			response.getWriter().print("            Friends posts : "+result);
			
			
		}
		catch (Exception e) {
		e.printStackTrace();
	
		response.sendRedirect("http://localhost:8080/bbb/login");
		}
	}
	
}
