package facebook.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import facebook.DatabaseConnection;
import facebook.DAO.postDAO;

@WebServlet("/uploadpost")
@MultipartConfig(maxFileSize = 999999999)
public class uploadpost extends HttpServlet {
	

	   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		   	
		   RequestDispatcher view = request.getRequestDispatcher("upload.jsp");
		   view.forward(request, response);
	   }
	
	   protected void doPost(HttpServletRequest request, HttpServletResponse response)
			     throws ServletException, IOException {
				 javax.servlet.http.Cookie[] ck=request.getCookies();  
				 String username =  ck[1].getValue();
			    
			     InputStream inputStream = null; 
			    
			     Part filePart = request.getPart("photo");
			     if (filePart != null) {
			         inputStream = filePart.getInputStream();
			     }
			     int likes = 0;
			     int comments = 0;
			     Random rand = new Random(); 
				 int postid = rand.nextInt(9000000) + 1000000; 
			   
			 	
				Connection con;
				try {
					con = DatabaseConnection.initializeDatabase();
					System.out.println("after con "+inputStream);
					String message = null;
					
					PreparedStatement pstmtSave = con.prepareStatement("INSERT INTO post (postid,file,username,likes,comments) VALUES (?,?,?,?,?)");
					
					pstmtSave.setInt(1, postid);
					if (inputStream != null) {
				         pstmtSave.setBlob(2, inputStream);
				     }

					pstmtSave.setString(3, username);
					pstmtSave.setInt(4,likes);
					pstmtSave.setInt(5,comments);
				
					int row = pstmtSave.executeUpdate();
				     if (row > 0) {
				         System.out.println("File uploaded and saved into database");
				     	response.getWriter().print("Image Uploaded Successfully");
				     }

				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e) {
				
					e.printStackTrace();
				}
			 }
	

}
