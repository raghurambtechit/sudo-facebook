package facebook.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import facebook.DAO.postDAO;
import facebook.DAO.userDAO;

@WebServlet(value = "/addFriend") 
public class addFriend extends HttpServlet	{

	

		 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
		 {
			   	
			   RequestDispatcher view = request.getRequestDispatcher("addfriend.jsp");
			   view.forward(request, response);
			   
		   }
		 
		 
		 protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				javax.servlet.http.Cookie[] ck=request.getCookies();  
			 try {
					
				    String username =  ck[1].getValue() ; 
					String friendname =request.getParameter("friendName");

					userDAO dao = new userDAO();
					boolean addFriend =  dao.addFriend(username,friendname);
				
					if(addFriend == true) {
						response.getWriter().print(friendname +" IS your New Friend");
					}	
				}
				catch (Exception e) {
				
					e.printStackTrace();
				
				}
			}
	}
	
	
	

	
